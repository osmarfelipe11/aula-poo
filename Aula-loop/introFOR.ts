namespace introFOR
{
    for(let i = 0; i < 5; i++)
    {
        console.log(`Bloco ${i}.`);
    }

    // mostrar número ímpar
    for(let i = 0; i < 1000; i++)
    {
        if(i%2 != 0){
            console.log(`O número ${i} é ímpar.`);
        }
    }

    // tabuada 
    let n = 3;
    for(let i = 0; i < 10; i++)
    {
        console.log(`${i} x ${n} = ${i*n}.`)
    }
}