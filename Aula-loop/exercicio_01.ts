//Escreva um programa TypeScript que imprima todos os números primos de 1 a 53 usando a função while

namespace exercicio1
{
    let numeros: number = 1;
    let aux: number;
    let primo: boolean;

    while(numeros <= 53){
        primo = true;
        aux = 2;
            while(aux < numeros){
                if(numeros % aux == 0){
                    primo = false;
                    break;
                }
                aux++
            }
        if(primo == true){
            console.log(numeros);
        }
        numeros++
    }
}