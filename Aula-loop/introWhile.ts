namespace introwhile
{
    let contador: number;
    contador = 0;

        // fazer enquanto for verdadeiro
    while(contador <= 10){

        console.log(`O valor do contador é ${contador}`);

        contador++

    }
        // fazer pelo menos uma vez.

    let contador1: number = 0;

    do{
        console.log(`do while; Contador = ${contador1}`)
        contador1++
    } while(contador1 <= 10)
}