/* Escreva um programa que pergunte ao usuário qual a sua cor favorita e exiba uma mensagem de acordo com a cor escolhida */

namespace exercicio_02
{
    let cor: string;
    console.log("Qual sua cor favorita?");

    cor = "Vermelho";
    
    switch(cor)
    {
        case "Vermelho": console.log("A cor vermelha, de modo geral, representa o guerreiro ou mártir, indica coragem e força.");
                        break;
        case "Verde": console.log("A cor verde significa esperança, liberdade, saúde e vitalidade.");
                    break;
        case "Amarelo": console.log("A cor amarela significa luz, calor, descontração, otimismo e alegria.");
                        break;
        case "Azul": console.log("A cor azul significa tranquilidade, serenidade, harmonia e espiritualidade, mas também está associada à frieza, monotonia e depressão.");
                    break;
        default: console.log("Não sei sobre essa cor.");
    }
}