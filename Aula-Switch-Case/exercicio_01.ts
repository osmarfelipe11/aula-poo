/* Escreva um programa que pergunte ao usuário qual o seu nível
de conhecimento em TypeScript e exiba uma mensagem de acordo com o nível escolhido */

namespace exercicio_01 
{
    let nivel: string;

    console.log("Qual seu nível de conhecimento em TypeScript.");

    nivel = "Baixo"; // Baixo; Basico; Avançado.

    switch(nivel)
    {
        case "Baixo": console.log("Você tem que estudar.");
                        break;
        case "Basico": console.log("Você ainda tem que estudar mais.");
                        break;
        case "Avançado": console.log("Parabéns! Você concluiu o curso.");
                        break;
        default: console.log("Níveis: Baixo, Basico e Avançado.");
    }
}