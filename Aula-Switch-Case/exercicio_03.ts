// Escreva um programa que pergunte ao usuário qual o seu sabor de sorvete favorito e exiba uma mensagem de acordo com o sabor escolhido

namespace exercicio_03
{
    let sabor: string;

    console.log("Qual seu sabor de sorvete favorito?");

    sabor = "Creme";

    switch(sabor)
    {
        case "Creme": console.log("Sabor bom.");
                    break;
        case "Chocolate": console.log("Sabor não é tão bom.");
                        break;
        case "Morango": console.log("Sabor um pouco bom.");
                        break;
        case "Ninho trufado": console.log("Sabor muito bom.")
                            break;
        default: console.log("Não conheço esse sabor.")
    }
}