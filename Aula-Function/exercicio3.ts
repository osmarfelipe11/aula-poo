/* Crie uma função que receba um array de números como parâmetro e retorne a soma desses números. */

namespace exercicio3 {
    function somaArray(nums:number[] = []){
        let soma = nums.reduce(function(total, num) {
            return total + num;
          });
        
          return soma;
    }

    console.log(somaArray([13, 3, 20, 18]));
    
}