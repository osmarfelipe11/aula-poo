/* Crie uma função que receba um número como parâmetro e retorne "par" se o número for par e "ímpar" se o número for ímpar. */

namespace exercicio4 {
    function parOuImpar(num:number){
        let numParOuImpar: string;
        if (num % 2 == 0){
            numParOuImpar = "O número é par";
        }
            else {
                numParOuImpar = "O número é ímpar"
            }
    return numParOuImpar;
    }

    console.log(parOuImpar(2));
    
}