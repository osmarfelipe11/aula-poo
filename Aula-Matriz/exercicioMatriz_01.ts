/*Crie uma matriz 3x3 com números aleatórios entre 0 e 10. Em seguida, crie um código que calcule e mostre o maior valor
encontrado na matriz.*/

namespace exercicioMatriz_01{

    let matriz: number[][] = Array.from({length: 3}, () => Array(3).fill(1));

    for(let i = 0; i < matriz.length; i++){
        for(let j = 0; j < matriz[i].length; j++){
            matriz[i][j] = Math.floor(Math.random() * 10);
        }
    }
    console.table(matriz);

    let maior: number = matriz[0][0];

    matriz.forEach((row) => {
        row.forEach((col) => {
            if(maior < col){
                maior = col;
            }
        })
    })

    console.log("Maior " + maior);

// Menor ------------------------------------------------

    let menor: number = matriz[0][0];

    matriz.forEach((row) => {
        row.forEach((col) => {
            if(menor > col){
                menor = col;
            }
        })
    })

    console.log("Menor " + menor);
}