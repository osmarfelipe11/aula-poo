/*Crie uma matriz 5x5 com números aleatórios entre 0 e 100. Em seguida, crie um código que calcule e mostre a soma de 
todos os valores da diagonal principal.*/

namespace exercicioMatriz_03 {
    let matriz: number[][] = Array.from({length: 5}, () => Array(5).fill(1));

    for(let i = 0; i < matriz.length; i++){
        for(let j = 0; j < matriz[i].length; j++){
            matriz[i][j] = Math.floor(Math.random() * 10);
        }
    }
}