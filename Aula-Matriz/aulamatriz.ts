namespace aulaMatriz
{
    let matriz: number[][] = [
        [5, 3, 7],//linha 1
        [3, 2, 4],//linha 2
        [8, 1, 6, 3, 5]//linha 3
    ];

    let elemento: number = matriz[1][2]; // 4
    console.log(elemento);
    console.log(matriz);
    console.table(matriz);

    for(let i = 0; i < matriz.length; i++){
        for(let j = 0; j < matriz[i].length; j++){
            console.log(matriz[i][j]);
        }
    }

    let matriz2: number[][] = Array.from({length: 3}, () => Array(4).fill(0));

    console.table(matriz2);

    for(let i = 0; i < matriz2.length; i++){
        for(let j = 0; j < matriz2[i].length; j++){
            matriz2[i][j] = Math.floor(Math.random() * 100);
        }
    }

    console.table(matriz2);

    matriz.forEach(row => {
        row.forEach(col => {
            console.table(col);})
    })

}