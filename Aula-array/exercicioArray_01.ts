//Crie um array com 5 números. Em seguida, use um loop for para iterar sobre o array e exibir a soma de todos os números.

namespace exercicioArray{
    let nums: number[] = [3, 5, 11, 2, 10];
    let somaNums: number = 0;

    for(let i = 0; i < nums.length; i++){
        somaNums += nums[i];
    }
    console.log(`A soma dos números é: ${somaNums}.`);
}