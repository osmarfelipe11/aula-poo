// Crie um array com 6 números. Em seguida, use o método filter() para criar um novo array contendo apenas os números ímpares.

namespace exercicioArray_04{

    let numeros: number[] = [1, 2, 3, 4, 5, 6];

    let impares = numeros.filter(function(num){
        return num %2 != 0;
    });

    console.log(impares);

}