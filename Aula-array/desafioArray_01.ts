/*Dado um array de objetos livros, contendo os campos titulo e autor, crie um programa em TypeScript que utilize a função filter()
para encontrar todos os livros do autor com valor "Autor 3". Em seguida, utilize a função map() para mostrar apenas os títulos
dos livros encontrados. O resultado deve ser exibido no console.*/

namespace desafioArray_filter_map {

    let livros: any[] = [{titulo: 'livro 1', autor: 'Autor 1'}, {titulo: 'livro 2', autor: 'Autor 2'}, {titulo: 'livro 3', autor: 'Autor 3'}, {titulo: 'Livro 4', autor: 'Autor 4'}];

    let autor3 = livros.filter(function(livro){
        return livro.autor === 'Autor 3';
    });

    let tituloAutor3 = autor3.map(function(titulos3){
        return titulos3.titulo;
    });

    console.log(tituloAutor3);
}