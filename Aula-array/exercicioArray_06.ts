/*Crie um vetor chamado "alunos" contendo três objetos, cada um representando um aluno com as seguintes propriedades:
"nome" (string), "idade" (number) e "notas" (array de números). Preencha o vetor com informações ficticias.
Em seguida, percorra o vetor utilizando a função "forEach" e para cada aluno, calcule a média das notas e imprima o resultado na tela, juntamente
com o nome e a idade do aluno.*/

interface Aluno {
    nome: string;
    idade: number;
    notas: number[];
}

namespace exercicioArray_06 {

    const alunos: Aluno[] = [
        {nome: 'Fulano', idade: 15, notas:[5, 6, 8]},
        {nome: 'Ciclano', idade: 14, notas:[8, 7, 9]},
        {nome: 'Deuflano', idade: 16, notas:[4, 6, 7]}
    ];

    /*alunos.forEach(function(aluno){
        let media: number = 0;
        media = (aluno.notas[0] + aluno.notas[1] + aluno.notas[2]) / aluno.notas.length;

        console.log('-------------------------');
        console.log(`Nome do aluno: ${aluno.nome} \n Idade do aluno: ${aluno.idade} \n Média das notas: ${media}`);
    });*/

    /*alunos.forEach(function(aluno){

        let media: number = 0;
        let soma: number = 0;

            aluno.notas.forEach(function(nota){
                soma += nota;
            })

        media = soma / aluno.notas.length;

        console.log('-------------------------');
        console.log(`Nome do aluno: ${aluno.nome} \n Idade do aluno: ${aluno.idade} \n Média das notas: ${media}`);
    });*/

    alunos.forEach(function(aluno){
        let media = aluno.notas.reduce(function(total, nota){
            return total + nota;
        }) / aluno.notas.length;
        
        console.log('-------------------------');
        console.log(`Nome do aluno: ${aluno.nome} \n Idade do aluno: ${aluno.idade} \n Média das notas: ${media}`);
    });

}