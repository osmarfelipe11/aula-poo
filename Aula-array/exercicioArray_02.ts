/*Crie um array com 3 nomes de frutas. 
Em seguida, use um loop while para iterar sobre o array e exibir cada fruta em uma linha separada.*/

namespace exercicioArray_02{
    let frutas: string[] = ["Pera", "Banana", "Mamão"];
    let contador: number = 0;
    while(contador < frutas.length){
        console.log(`${frutas[contador]}`);
        contador++;
    }
}
