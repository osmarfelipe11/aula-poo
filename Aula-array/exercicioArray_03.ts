/*Crie um array com 4 objetos, cada um representando um livro com as propriedades titulo e autor.
Em seguida, use o método map() para criar um novo array contendo apenas os títulos dos livros.*/

namespace exercicioArray_03 {
    let livros: any[] = [{titulo: "Harry Potter", autor: "J.K. Rowling"}, {titulo: "Percy Jackson e os Olimpianos", autor: "Rick Riordan"}, 
    {titulo: "The Hitchhiker's Guide to the Galaxy", autor: "Douglas Adams"}, {titulo: "O Senhor dos Anéis", autor: "J. R. R. Tolkien"}];

    let titulos = livros.map(function(livro){
        return livro.titulo;
    });

    console.log(titulos);

}
